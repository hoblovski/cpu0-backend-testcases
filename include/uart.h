#ifndef UARTOUT_H
#define UARTOUT_H

#define UART_OUT 0x300000

#define put_uart(v) (*(unsigned*) UART_OUT = (v));

#endif // UARTOUT_H
