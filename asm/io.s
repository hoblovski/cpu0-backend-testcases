.globl main
main:
  lui $s0, 0x30
  # s0 = 0x300000
  lui $t0, 0xBEEF
  ori $t0, $t0, 0xBEEF
  # direct output here
  sto $t0, 0x90($s0)

# enable interrupts
  addiu $t0, $zr, 32  # OUTEN
  or $fr, $fr, $t0
  addiu $t0, $zr, 128 # CLKEN
  or $fr, $fr, $t0
	lui	$t0, %hi(vector)
	ori	$t0, $t0, %lo(vector)
  sto $t0, 0x20($s0)       # IRQ HANDLER
  addiu $t0, $zr, 2 # GIE
  or $fr, $fr, $t0

  addiu $a0, $zr, 1
spin:
  beq $a0, $zr, t_uartout
  beq $zr, $zr, spin

vector:
  lui $t0, 0xC0FF
  ori $t0, $t0, 0xFFEE
  sto $t0, 0x90($s0)
  addiu $t0, $zr, 256
  and $t0, $fr, $t0
  beq $t0, $zr, $next0
  beq $zr, $zr, uartin
$next0:
  addiu $t0, $zr, 64
  and $t0, $fr, $t0
  beq $t0, $zr, $next1
  beq $zr, $zr, uartout
$next1:
  beq $zr, $zr, spin

uartin:
  lui $t0, 0x1234
  sto $t0, 0x90($s0)
  loa $s1, 0x10($s0)
  sto $s1, 0x90($s0)
  add $a0, $zr, $zr # let goto t_uartout when eret

  addiu $t0, $zr, -257  # ~UART1IN    intr deassertion
  and $fr, $fr, $t0
  addiu $t0, $zr, -1025  # ~UART1INRDY  input enabled again
  and $fr, $fr, $t0
  addiu $t0, $zr, 4     # eret
  or $fr, $fr, $t0  

uartout:
  lui $t0, 0xABCD
  sto $t0, 0x90($s0)
  addiu $t0, $zr, -65  # ~UART1OUT
  and $fr, $fr, $t0
  addiu $t0, $zr, 4
  or $fr, $fr, $t0  # eret back to spin

t_uartout:
  addiu $s1, $s1, 1
  sto $s1, 0($s0)     # put data ready
  addiu $t0, $zr, -513  # ~UART1OUTRDY.
  # write this bit to show I want to output something
  and $fr, $fr, $t0       # goes to vector then uartout
  addiu $a0, $zr, 1
  beq $zr, $zr, spin
