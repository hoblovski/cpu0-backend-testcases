
.globl main
main:
  lui $t0, 0x30
  # t0 = 0x300000
  lui $t1, 0xDEAD
  ori $t1, $t1, 0xBEEF
  sto $t1, 0($t0)

spin:
  beq $zr, $zr, spin
