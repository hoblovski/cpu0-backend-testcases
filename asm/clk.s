.globl main
main:
  lui $s0, 0x30
  # s0 = 0x300000
  lui $t0, 0xDEAD
  ori $t0, $t0, 0xBEEF
  sto $t0, 0($s0)

# enable interrupts
  addiu $t0, $zr, 8  # CLKEN
  or $fr, $fr, $t0
  addiu $t0, $zr, -17 # ~CLK
  and $fr, $fr, $t0
  addiu $t0, $zr, 100
  sto $t0, 0x30($s0)
  addiu $t0, $zr, 2 # GIE
  or $fr, $fr, $t0

spin:
  beq $zr, $zr, spin
