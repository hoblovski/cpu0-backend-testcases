#include "uart.h"
#include <stdarg.h>

void vput(unsigned n, va_list arglist);

void put(unsigned n, ...)
{
	va_list arglist;
	va_start(arglist, n);
	vput(n, arglist);
	va_end(arglist);
}

void vput(unsigned n, va_list arglist)
{
  while (n--)
    put_uart(va_arg(arglist, unsigned));
  put_uart(~0u);
}

int main()
{
  put(4, 2, 3, 5, 7);
  while (1);
}
