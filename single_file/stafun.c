#include "uart.h"

static unsigned gcd(unsigned a, unsigned b)
{
  unsigned c;
  while (b != 0) {
    if (a < b) {
      c = a; a = b; b = c;
    }
    a -= b;
  }
  return a;
}

static unsigned ga = 8;
static unsigned gb = 6;
unsigned gc = 9106370;
unsigned gd = 990693;

int main()
{
  put_uart(gcd(ga, gb)); // should be 2
  put_uart(gcd(gc, gd)); // should be 10007
  while (1);
  return 0;
}
