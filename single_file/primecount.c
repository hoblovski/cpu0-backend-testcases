/*
 * Counts how many prime numbers there are below 10000.
 *
 * Expected answer: 1229
 */
#include "uart.h"

#define MAX_N 10000

unsigned isprime[MAX_N];

int main()
{
  for (unsigned i = 0; i < MAX_N; i++)
    isprime[i] = 1;
  isprime[0] = isprime[1] = 0;
  unsigned np = 0;
  for (unsigned i = 2; i < MAX_N; i++)
    if (isprime[i]) {
      np++;
      for (unsigned j = i+i; j < MAX_N; j += i)
        isprime[j] = 0;
    }
  put_uart(np);
  while (1);
}
