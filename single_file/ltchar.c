#include "uart.h"

unsigned sltSP(unsigned a)
{
  if (a < ' ')
    return 1;
  else
    return 0;
}

int main()
{
  put_uart(sltSP(0));
  put_uart(sltSP(31));
  put_uart(sltSP(32));
  put_uart(sltSP(33));
  while (1);
}
