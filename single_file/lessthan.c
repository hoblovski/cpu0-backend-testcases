#include "uart.h"

unsigned slt2(unsigned a)
{
  if (a < 2)
    return 1;
  else
    return 0;
}

int main()
{
  put_uart(slt2(0));
  put_uart(slt2(1));
  put_uart(slt2(2));
  put_uart(slt2(3));
  while (1);
}
