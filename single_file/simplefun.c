#include "uart.h"

unsigned a()
{
  return 0xDEADBEEF;
}

unsigned b()
{
  return 0x00C0FFEE;
}

unsigned c()
{
  return 0x0BADDEEA;
}

int main()
{
  put_uart(a());
  put_uart(b());
  put_uart(c());
  while (1) ;
}
