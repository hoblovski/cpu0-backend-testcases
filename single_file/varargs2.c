#include <stdarg.h>
#include "uart.h"

unsigned sum(unsigned amount, ...)
{
  unsigned i = 0;
  unsigned val = 0;
  unsigned sum = 0;
  va_list vl;
  va_start(vl, amount);
  for (i = 0; i < amount; i++) {
    val = va_arg(vl, unsigned);
    sum += val;
  }
  va_end(vl);
  return sum;
}

int main()
{
  put_uart(sum(3, 15531, 21331, 97213121)); // should be 97249983
  while (1);
}
