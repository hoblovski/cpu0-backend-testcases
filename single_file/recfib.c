#include "uart.h"

unsigned fib(unsigned n) {
  if (n < 2) return 1;
  return fib(n-1) + fib(n-2);
}

int main()
{
  for (unsigned i = 0; i < 15; i++)
    put_uart(fib(i));
  while (1);
}
