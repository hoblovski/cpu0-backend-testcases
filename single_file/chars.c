#include "uart.h"

typedef unsigned char uchar;

unsigned loadbyteu(const uchar* a)
{
  unsigned r = ((unsigned) a) & 3;
  unsigned rv = *(unsigned*) (((unsigned) a) & ~3);
  return (rv >> (r << 3)) & 0xFF;
}

void dumpord(const uchar* a)
{
  while (1) {
    unsigned ch = loadbyteu(a);
    if (!ch) break;
    put_uart(ch);
    a++;
  }
}

int main()
{
  dumpord((const uchar*) "Hello World");
  while (1);
}
