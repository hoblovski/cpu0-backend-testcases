/*
 * Simple write some 0xDEADBEEF's to uart out.
 *
 * Expected: 13 DEADBEEF's
 */

#include "uart.h"

int main()
{
  for (unsigned i = 0; i < 13; i++)
    put_uart(0xDEADBEEF);
  while (1) ;
  return 0;
}
