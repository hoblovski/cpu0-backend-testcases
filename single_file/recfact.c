#include "uart.h"

unsigned fact(unsigned i)
{
  if (i == 0)
    return 1;
  return i * fact(i-1);
}

int main()
{
  put_uart(fact(7));
}
