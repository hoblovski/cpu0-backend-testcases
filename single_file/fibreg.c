/*
 * Computes th fibonacci series using only 2 stack variables.
 *
 */

#include "uart.h"

int main()
{
  unsigned a = 0;
  unsigned b = 1;
  while (1) {
    put_uart(b);
    b += a;
    a = b - a;
  }
}
