#include "uart.h"

int main()
{
  unsigned a = 0x55555555;
  unsigned b = 0xcccccccc;
  put_uart(a);
  put_uart(b);
  put_uart(a | b);
  put_uart(a & b);
  put_uart(a ^ b);
  put_uart(a >> 4);   // should be a logic_right 4
  put_uart(b >> 4);   // also b logic_right 4
  while (1);
}
