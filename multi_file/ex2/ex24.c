unsigned count_word(unsigned* s, unsigned len, unsigned val)
{
  unsigned rv = 0;
  for (int i = 0; i < len; i++)
    if (s[i] == val)
      rv++;
  return rv;
}

