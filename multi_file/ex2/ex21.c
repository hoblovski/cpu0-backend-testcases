#include "uart.h"

#include "ex22.h"
#include "ex23.h"

extern unsigned count_word(unsigned* s, unsigned len, unsigned val);

int main()
{
  fill_00000000[FILL_00000000_SZ - 10] = 0xabcd;
  fillword(fill_deadbeef, FILL_DEADBEEF_SZ, 0xDEADBEEF);
  put_uart(count_word(fill_deadbeef, FILL_DEADBEEF_SZ, 0xDEADBEEF));
  fillword(fill_abcdef12, FILL_ABCDEF12_SZ, 0xABCDEF12);
  put_uart(count_word(fill_abcdef12, FILL_ABCDEF12_SZ, 0xABCDEF12));
  fillword(fill_00000000, FILL_00000000_SZ, 0x00000000);
  put_uart(count_word(fill_00000000, FILL_00000000_SZ, 0x00000000));
  while (1);
}
