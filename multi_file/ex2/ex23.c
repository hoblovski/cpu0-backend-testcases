#include "uart.h"

void fillword(unsigned* ptr, unsigned len, const unsigned val)
{
  while (len--) {
    *(ptr++) = val;
  }
}
