#ifndef EX22_H
#define EX22_H

#define FILL_DEADBEEF_SZ 12
#define FILL_ABCDEF12_SZ 1212
#define FILL_00000000_SZ 121212

extern unsigned fill_deadbeef[];
extern unsigned fill_abcdef12[];
extern unsigned fill_00000000[];


#endif // EX22_H
