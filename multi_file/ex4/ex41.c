#include "uart.h"
#include "ex42.h"

int main()
{
  pt a = {3, 4};
  pt b = {5, 6};
  pt c;
  add(&a, &b, &c);
  put(&c);
  mul(&a, &c, &c);
  put(&c);
  while (1);
}
