#ifndef EX42_H
#define EX42_H

#include "uart.h"

typedef struct {
  unsigned x;
  unsigned y;
} pt;

void add(pt* a, pt* b, pt* c);
void mul(pt* a, pt* b, pt* c);
void put(pt* a);

#endif // EX42_H
