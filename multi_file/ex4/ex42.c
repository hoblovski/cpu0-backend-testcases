#include "ex42.h"

void add(pt* a, pt* b, pt* c)
{
  c->x = a->x + b->x;
  c->y = a->y + b->y;
}

void mul(pt* a, pt* b, pt* c)
{
  c->x = a->x * b->x;
  c->y = a->y * b->y;
}

void put(pt* a)
{
  put_uart(~0u);
  put_uart(a->x);
  put_uart(a->y);
  put_uart(~0u);
}
